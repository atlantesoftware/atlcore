##########
Atl Framework
##########

A Django framework for managing hierarchical content, pages of content, in
multiple languages and/or on multiple sites.

Atl Framework handles the navigation rendering for you with clean, slug based URLs,
and this navigation can be extended by custom Django applications.

More information on `our website <http://www.atlantesoftware.com>`_. 

*************
Documentation
*************

Please head over to our `documentation <http://docs.atlantesoftware.com/>`_ for all
the details on how to install, extend and use the Atl Framework.


*******
Credits
*******

* Credits Here