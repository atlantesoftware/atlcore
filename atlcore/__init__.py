# -*- coding: utf-8 -*-

VERSION = (2, 0, 0, 'alpha', 32)

def get_version(version=None):
    # Only import if it's actually called.
    from atlcore.utils.version import get_version
    return get_version(version)
