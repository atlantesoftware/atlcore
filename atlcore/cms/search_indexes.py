import datetime
from haystack import indexes

from django.template import RequestContext
from django.utils.html import strip_tags
from django.test.client import RequestFactory
from cms.models.pagemodel import Page
from cms.models.pluginmodel import CMSPlugin


class PageIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=False)
    pub_date = indexes.DateTimeField(model_attr='publication_date', null=True)
    login_required = indexes.BooleanField(model_attr='login_required')
    url = indexes.CharField(model_attr='get_absolute_url')
    title = indexes.CharField(model_attr='get_title')

    def prepare(self, obj):
        self.prepared_data = super(PageIndex, self).prepare(obj)
        plugins = CMSPlugin.objects.filter(placeholder__in=obj.placeholders.all())
        text = ''
        rf = RequestFactory()
        request = rf.get("/")
        request.session = {}
        for plugin in plugins:
            instance,plugin_type = plugin.get_plugin_instance()
            if hasattr(instance, 'search_fields'):
                text += ''.join(getattr(instance, field) for field in instance.search_fields)
            if getattr(instance, 'search_fulltext', False) or getattr(plugin_type, 'search_fulltext', False):
                text += strip_tags(instance.render_plugin(context=RequestContext(request))) + u' '
        text += obj.get_meta_description() or u''
        text += obj.get_title() or u''
        text += obj.get_meta_keywords() if hasattr(obj, 'get_meta_keywords') and obj.get_meta_keywords() else u''
        self.prepared_data['text'] = text
        return self.prepared_data

    def get_model(self):
        return Page

    def index_queryset(self, using=None):
        return Page.objects.published().filter(publisher_is_draft=False).distinct()
