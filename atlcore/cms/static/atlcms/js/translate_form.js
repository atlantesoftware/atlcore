/**
 * Created with PyCharm.
 * User: allen
 * Date: 4/7/14
 * Time: 5:30 PM
 * To change this template use File | Settings | File Templates.
 */
(function($) {

	// global functions
	trigger_lang_button = function(e, url) {
		// also make sure that we will display the confirm dialog
		// in case users switch tabs while editing plugins
        window.location = url;
    }
})(jQuery);