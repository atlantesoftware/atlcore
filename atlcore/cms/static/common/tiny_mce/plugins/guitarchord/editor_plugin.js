(function() {

	tinymce.create('tinymce.plugins.GuitarchordPlugin', {
		
		init : function(ed, url) {
			// INSERT CHORD
			ed.addButton('insert_lyric_button', {
	            title : 'Poner acorde',
	            image : url + '/img/ui_insert_chord_icon.jpg',
	            onclick : function() {
	                ed.focus();	                
	                var chord = jQuery('#id_lyrics_chordListBox').val();
                    var variation = jQuery('#id_lyrics_variationListBox').val();

                    if (chord !== '' && variation !== '') {
                    	if (variation === 'major') {
                    		variation = '';
                    	}
                    	var content = ed.selection.getContent().trim() !== '' ? ed.selection.getContent() : '&nbsp;';
                    	var html_tag = '<chord class="'  + chord + '' + variation +'">'+ content +'</chord>';                   		
                     	ed.selection.setContent(html_tag);
                    }
	            }
	        });
	         
	        //DELETE CHORD
	        ed.addButton('delete_lyric_button', {
	            title : 'Borrar acorde',
	            image : url + '/img/ui_delete_chord_icon.jpg',
	            onclick : function() {
	                ed.focus();	                
					if (ed.selection.getContent() !== '') {						
						//console.log(ed.selection.getContent());
						//console.log(jQuery(ed.selection.getContent()).text());
						
						if (ed.selection.getContent().indexOf('<chord') >= 0) {
							var text = jQuery(ed.selection.getContent()).text();
							ed.selection.setContent(text);	
						} else {
							alert('Seleccione un texto dentro de un acorde');	
						}
					} else {
						alert('Seleccione un texto dentro de un acorde');
					} 
					
	            }
	        });
			
		},

		createControl : function(n, cm) {
			
			switch (n) {
                case 'chordListBox':
                    var mlb = cm.createListBox('chordListBox', {
                         title : 'Acordes',
                         onselect : function(v) {
                             //tinyMCE.execCommand('mceInsertContent',false,v);
                             //tinyMCE.activeEditor.windowManager.alert('Value selected:' + v);
                         }
                    }, tinymce.ui.NativeListBox);
                    mlb.add('C', 'C');
					mlb.add('C#', 'Ccharp');
					mlb.add('D', 'D');
					mlb.add('D#', 'Dcharp');
					mlb.add('E', 'E');
					mlb.add('F', 'F');
					mlb.add('F#', 'Fcharp');
					mlb.add('G', 'G');
					mlb.add('G#', 'Gcharp');
					mlb.add('A', 'A');
					mlb.add('A#', 'Acharp');
					mlb.add('B', 'B');
                    return mlb;
                    
                case 'variationListBox':
                    var mlb = cm.createListBox('variationListBox', {
                         title : 'Variaciones',
                         onselect : function(v) {
                         }
                    }, tinymce.ui.NativeListBox);
                    
					mlb.add('Mayor', 'major');
					mlb.add('Menor', '-');
					mlb.add('dim', 'dim');
					mlb.add('dim7', 'dim7');
					mlb.add('aug', 'aug');
					//mlb.add('sus2', 'sus2');
					//mlb.add('sus4', 'sus4');
					mlb.add('7', '7');
					mlb.add('maj7', 'maj7');
					mlb.add('m7', 'm7');
					mlb.add('mmaj7', 'mmaj7');
					mlb.add('m7b5', 'm7b5');
					mlb.add('7sus4', '7sus4');
                    return mlb;
                }
         	return null;			
		},

		getInfo : function() {
			return {
				longname : 'Example plugin',
				author : 'Some author',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('guitarchord', tinymce.plugins.GuitarchordPlugin);
})();