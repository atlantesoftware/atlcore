//tinyMCEPopup.requireLangPack();

var ImageBrowserDialog = {
	
	init : function(ed, url) {
		//var f = document.forms[0];
		var that = this;
		// Get the selected contents as text and place it in the input
		//f.someval.value = tinyMCEPopup.editor.selection.getContent({format : 'text'});
		//f.somearg.value = tinyMCEPopup.getWindowArg('some_custom_arg');
		

		$(".node-content").live("click", function(){
			var pk = $('input',this).val();
			that.update(pk);
		});
		
		$("img").live("click", function(){
			var picture_url = $(this).parent().find(':hidden').val();// attr('src');
			window.opener.document.getElementById('src').value = picture_url; 
			//tinyMCEPopup.close();
			window.close();
		});

		
		this.update();
		
	},
	
	update : function(pk) {
		
		$.ajax({
			type: "GET",
			url: "/atlcms/dialog/imagebrowser/",
			data: {id : pk},
			success: function(data) {
				$('#gallery-content').html(data);												
			}			
		});
	},

	insert : function() {
		// Insert the contents from the input into the document
		//tinyMCEPopup.editor.execCommand('mceInsertContent', false, document.forms[0].someval.value);
		//tinyMCEPopup.close();
	}
};

ImageBrowserDialog.init();
//tinyMCEPopup.onInit.add(ImageBrowserDialog.init, ImageBrowserDialog);
