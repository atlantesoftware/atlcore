#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    {% if atl_path %}
    atlpath = "{{atl_path}}"
    if atlpath not in sys.path:
        sys.path.insert(0, atlpath)
    {% endif %}
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

    from atlcore.management import execute_from_command_line

    execute_from_command_line(sys.argv)
