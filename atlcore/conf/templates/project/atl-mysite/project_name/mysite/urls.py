from django.conf.urls import *
from django.contrib import admin
from django.views.static import serve
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^jsi18n/(?P<packages>\S+?)/$', 'django.views.i18n.javascript_catalog'),
)

urlpatterns += staticfiles_urlpatterns()

from atlcore.cms.admin import panel
urlpatterns += patterns('',
    (r'^atlcms/', include(panel.urls)),
)

urlpatterns += i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('cms.urls')), # <--------- include the django cms urls via i18n_patterns
)


if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns

urlpatterns += patterns('',
    (r'^%s/(?P<path>.*)$' % settings.MEDIA_URL[1:-1], serve, {'document_root': settings.MEDIA_ROOT}),
    (r'^captcha/', include('captcha.urls')),
)

urlpatterns += patterns('',
    (r'^aspects/', include('atlcore.aspect.urls')),
)

urlpatterns += patterns('',
    (r'^relations/', include('atlcore.relations.urls')),
)

urlpatterns += patterns('',
    (r'^tinymce/', include('tinymce.urls')),
)
