# -*- coding: utf-8 -*-
import sys, os
PROJECT_ROOT = os.path.dirname(__file__)
PROJECT_ROOT_PARENT = os.path.split(PROJECT_ROOT)[0]
PROJECT_NAME = os.path.split(PROJECT_ROOT)[1]

DEBUG = True
#DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.path.join(PROJECT_ROOT, "{{project_name}}.db"),                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {
                    #'init_command': 'SET storage_engine=MyISAM',
        }
    },
}

