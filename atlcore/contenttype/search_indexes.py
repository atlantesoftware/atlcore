# -*- coding: utf-8 -*-
import datetime
from haystack import indexes
from atlcore.contenttype.models import News, Video, Document


class NewsIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return News

    def index_queryset(self,using=None):
        """
        This is used when the entire index for model is updated, and should only include
        public entries
        """
        return self.get_model().objects.filter(updated_on__lte=datetime.datetime.now(), state='Public')

class VideoIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Video

    def index_queryset(self,using=None):
        """
        This is used when the entire index for model is updated, and should only include
        public entries
        """
        return self.get_model().objects.filter(updated_on__lte=datetime.datetime.now(), state='Public')

class DocumentIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Document

    def index_queryset(self,using=None):
        """
        This is used when the entire index for model is updated, and should only include
        public entries
        """
        return self.get_model().objects.filter(updated_on__lte=datetime.datetime.now(), state='Public')
