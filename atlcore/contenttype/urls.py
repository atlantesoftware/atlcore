from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
            url(r'^(?P<id>[a-fA-F0-9]{32})/$', 'atlcore.contenttype.views.details_view', name='content_details'),
            url(r'^(?P<slug>[a-zA-Z0-9-]+)/$', 'atlcore.contenttype.views.details_view', name='content_details_with_slug'),
            url(r'^(?P<slug>[a-zA-Z0-9-]+)$', 'atlcore.contenttype.views.details_view', name='content_details_with_slug2'),
            url(r'^(?P<id>[a-fA-F0-9]{32})/(?P<slug>[a-zA-Z0-9-]+)/+$', 'atlcore.contenttype.views.details_view', name='content_details_with_slug'),
            #url(r'^(?P<slug>[a-zA-Z0-9-]+)/$', views.details_view, name='%s_details1' %self.opts.module_name),
            
            url(r'^(?P<id>[a-fA-F0-9]{32})/(?P<slug>[a-zA-Z0-9-]+)/(?P<view_name>.+)/$', 'atlcore.contenttype.views.details_view', name='content_details_with_view'),
            #url(r'^ajax/(?P<id>[a-fA-F0-9]{32})/(?P<view_name>.+)/$', self.ajax_details_view, name='%s_ajax_details' %self.opts.module_name),
        )