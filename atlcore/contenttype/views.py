# Create your views here.
# -*- coding: utf-8 -*-
from django.template.context import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response
from atlcore.contenttype.models import Folder, Node
from atlcore.site.dublincore import DublinCore
from django.shortcuts import get_list_or_404
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect


def details_view(request, id=None, slug=None, view_name=None):
    if id:
        atl_node = get_object_or_404(Node, id=id).get_instance()
    elif slug:
        atl_node = get_list_or_404(Node, slug=slug)[0].get_instance()

    for node in atl_node.get_relation_objects('translation'):
        if node.language == request.LANGUAGE_CODE:
            url = node.content_type.name
            if id and slug:
                url = '/%s/%s/%s/' % (url, node.id, node.slug)
            elif id and not slug:
                url = '/%s/%s/' % (url, node.id)
            else:
                url = '/%s/%s/' % (url, node.slug)
            return HttpResponseRedirect(url)

    brothers = [i.get_instance() for i in
                Node.objects.filter(parent=atl_node.parent, content_type__model=atl_node.class_name_instance).order_by(
                    '-order')]
    try:
        next_content = Node.objects.filter(parent=atl_node.parent, order__lt=atl_node.order).order_by('-order')[0]
    except:
        next_content = None
    try:
        previous_content = Node.objects.filter(parent=atl_node.parent, order__gt=atl_node.order).order_by('order')[0]
    except:
        previous_content = None

    dc = DublinCore()
    dc.title = atl_node.title
    dc.description = atl_node.meta_description
    dc.keywords = atl_node.subject
    dc.author = atl_node.creator
    dc.copyright = atl_node.rights
    context = {}
    context['atl_node'] = atl_node
    context['brothers'] = brothers
    context['previous_content'] = previous_content
    context['next_content'] = next_content
    context['dc'] = dc
    if view_name:
        template = 'atlcontenttype/%s.html' % view_name
    else:
        template = None;
    template_list = [
        'atlcontenttype/%s.html' % context['atl_node'].class_name_instance,
        'atlcontenttype/content.html',
    ]

    return render_to_response(template or template_list,
                              context,
                              context_instance=RequestContext(request))
