__author__ = 'allenson'
import sys

from django.core import management


def get_commands():
    """
    Returns a dictionary mapping command names to their callback applications.

    This works by looking for a management.commands package in django.core,
    plus management.commands package in atlcore and
    in each installed application -- if a commands package exists, all commands
    in that package are registered.

    Core Django and ATL commands are always included. If a settings module has been
    specified, user-defined commands will also be included.

    The dictionary is in the format {command_name: app_name}. Key-value
    pairs from this dictionary can then be used in calls to
    load_command_class(app_name, command_name)

    If a specific version of a command must be loaded (e.g., with the
    startapp command), the instantiated module can be placed in the
    dictionary in place of the application name.

    The dictionary is cached on the first call and reused on subsequent
    calls.
    """
    #global management._commands
    if management._commands is None:
        management._commands = dict([(name, 'django.core') for name in management.find_commands(management.__path__[0])])

        #Adding ATL Commands
        management._commands.update(dict([(name, 'atlcore') for name in management.find_commands(__path__[0])]))

        # Find the installed apps
        from django.conf import settings
        try:
            apps = settings.INSTALLED_APPS
        except (management.ImproperlyConfigured,ImportError):
            # Still useful for commands that do not require functional settings,
            # like startproject or help
            apps = []

        # Find and load the management module for each installed app.
        for app_name in apps:
            try:
                path = management.find_management_module(app_name)
                management._commands.update(dict([(name, app_name)
                                       for name in management.find_commands(path)]))
            except ImportError:
                pass # No management module - ignore this app

    return management._commands


def execute_from_command_line(argv=None):
    """
    A simple method that runs a ManagementUtility.
    And loads django.core and atl commands by default
    """

    utility = management.ManagementUtility(argv)
    get_commands()
    utility.execute()
