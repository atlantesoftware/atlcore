#encoding=utf-8
import os
from optparse import make_option

from django.core.management.base import CommandError
from django.core.management.templates import TemplateCommand
from django.utils.crypto import get_random_string
from django.utils.importlib import import_module

import atlcore
from atlcore.utils import location

class Command(TemplateCommand):
    help = ("Creates an ATL project directory structure for the given "
            "project name in the current working directory or optionally in the "
            "given directory.")
    
    option_list = TemplateCommand.option_list + (
        make_option('--kind','-k',
                    action='store',type='choice',choices=['atl-mysite','atl-mysite2'],default='atl-mysite', dest='kind',
                    help='ATL Project Skeleton to be created ("atl-mysite","atl-mysite2") (default: "atl-mysite").'),
        make_option('--atlpath','-p',action='store_true',dest='atlpath',
            help='Injects ATL path to the Python path (in manage.py script) , e.g. "/home/djangoprojects/atlcore".'),
        )    
    
    def _inject_template_path(self,target,options):
        template = options.get('template')
        if template is None:
            kind = options.get('kind')
            project_container_folder = 'project_name' if location.is_atl_embedded(target) else ''
            template = os.path.join(atlcore.__path__[0], 'conf','templates','project',kind,project_container_folder)
            options['template'] = template
    
    def _inject_extra_extensions(self,options):
        extensions = options.get('extensions')
        html_ext = 'html'
        if html_ext not in extensions:
            options['extensions'].append(html_ext)
        
    def get_default_target(self):
        return os.getcwd()
        
    def _mutate_options(self,options):
        pass
        
    def handle(self, project_name=None, target=None, *args, **options):
        if project_name is None:
            kind = options.get('kind')
            if kind in ('atl-mysite','atl-mysite2'):
                project_name = "mysite"
            else:
                raise CommandError("you must provide a project name")

        # Check that the project_name cannot be imported.
        try:
            import_module(project_name)
        except ImportError:
            pass
        else:
            raise CommandError("%r conflicts with the name of an existing "
                               "Python module and cannot be used as a "
                               "project name. Please try another name." %
                               project_name)

        # Create a random SECRET_KEY hash to put it in the main settings.
        chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
        options['secret_key'] = get_random_string(50, chars)
        if target is None:
            target = location.get_project_default_base_path()
        self._inject_template_path(target,options)
        atlpath = options.get('atlpath')
        if atlpath :
            options['atl_path'] = location.get_atl_base_path()

        super(Command, self).handle('project', project_name, target, **options)
        