__author__ = 'rmc'

from django.contrib import admin
from models import ATLFinderContent
from forms import ATLFinderContentForm

class ATLFinderInline(admin.StackedInline):
    model = ATLFinderContent
    extra = 1
    form = ATLFinderContentForm