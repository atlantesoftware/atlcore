#coding=UTF-8
from django.utils.translation import ugettext as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from atlcore.contenttype.models import Node
from models import ATLFinder
from admin import ATLFinderInline
from forms import ATLFinderForm

# from django.db.models import get_model
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class ATLFinderPlugin(CMSPluginBase):
    model = ATLFinder
    name = _("ATLFinder Plugin")
    form = ATLFinderForm
    change_form_template = 'ATLFinderForm.html'
    inlines = [ATLFinderInline]

    fieldsets = (
        (_("Basic"), {
            'fields': ('title', 'format', 'model', 'container', 'order_by', 'show_paginator', 'quantity'),
        }),
        (_("Advanced"), {
            'classes': ('collapse', 'collapsed'),
            'fields': (
                'content_language', 'template','main_content', 'link_content', 'fields', 'aspect',
                'exclude'),
        }),

    )


    def render(self, context, instance, placeholder):

        self.render_template = "atlfinder/%s" % instance.template
        request = context['request']
        result = self.get_items_by_atlfinder_instance(instance)
        #for ip in instance.exclude.all():
        #    result = list(set(result) - set(self.get_items_by_atlfinder_instance(ip)))

        objects = []
        for obj in result:
            objects.append(obj.get_instance())

        fields = []
        for f in instance.fields.all():
            fields.append(f.name)

        context['instance'] = instance
        context['exclude'] = instance.exclude.all()
        context['exclude_fields'] = fields
        context['objects'] = objects

        if instance.show_paginator:
            paginator = Paginator(objects, instance.quantity)
            page = request.GET.get('page')
            try:
                context['objects'] = paginator.page(page)
            except PageNotAnInteger:
                context['objects'] = paginator.page(1)
            except EmptyPage:
                context['objects'] = paginator.page(paginator.num_pages)

        return context

    def get_items_by_atlfinder_instance(self, instance):
        objects = []
        if instance.format == 'c':
            set = instance.atlfindercontent_set.all()
            if len(set) > 0:
                for obj in set:
                    objects.append(Node.objects.get(id=obj.node_id))

        elif instance.format == 'l':
            args = {'state': 'Public', 'content_type__model': instance.model.lower()}
            # content_language
            if instance.content_language != '':
                args.update({'language': instance.content_language})
                # container
            if instance.container:
                args.update({'parent': instance.container})
                # aspect
            aspects = instance.aspect.all()
            if len(aspects) > 0:
                args.update({'aspect__in': aspects})
            if instance.order_by:
                objects = Node.objects.filter(**args).order_by('-order')
            else:
                objects = Node.objects.filter(**args).order_by('-date')
        return objects


plugin_pool.register_plugin(ATLFinderPlugin)