#coding=UTF-8
from django.utils.translation import ugettext as _
from django import forms
from models import ATLFinder, ATLFinderContent, ATLFinderFields
from django.contrib.admin.widgets import FilteredSelectMultiple
from atlcore.contenttype.models import Node
from utils.itersubclasses import itersubclasses

#load atlcore models fields
fields = {}
fields['Node'] = Node._meta.get_all_field_names()
for model in itersubclasses(Node):
    fields[model.__name__] = model._meta.get_all_field_names()
    for parent in model._meta.get_parent_list():
        fields[model.__name__] = [x for x in list(set(fields[model.__name__]) - set(parent._meta.get_all_field_names()))
                                  if not x.endswith('_ptr')]
OPTIONS = []
for type, group in fields.items():
    for attr in group:
        OPTIONS.append((attr, '%s - %s' % (type, attr)))
#end load atlcore models fields


class ATLFinderForm(forms.ModelForm):
    fields = forms.ModelMultipleChoiceField(ATLFinderFields.objects.all(),
                                            widget=FilteredSelectMultiple("fields", False,
                                                                          attrs={'rows': '10'}), required=False
    )
    template = 'ATLFinderForm.html'

    class Media:
        css = {
            'all': ('/media/css/widgets.css',),
        }
        js = ('/admin/jsi18n/',)

    class Meta:
        model = ATLFinder


class ATLFinderContentForm(forms.ModelForm):
    node_id = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = ATLFinderContent
        exclude = ['weight']