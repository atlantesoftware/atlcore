��            )         �     �     �     �     �     �     �     �     
          /     >  @   G     �     �     �     �     �  \   �            
   (     3     B     Y  -   _  R   �  $   �                 @  $  	   e     o     x     �     �     �  $   �     �     �            R   #     v     �     �     �  
   �  9   �     �  %   �          1     I     g  4   o  Y   �  i   �     h	  	   w	     �	        	                                           
                                                                                     Add Advanced Aspect to filter by Basic Cancel Container folder to filter by Display mode Home How many items to show per page Items per page Language Language to filter by. Leave unselected to include all languages List Mode Model Node Parent folder Please correct the error below. Please
                            correct the errors below. Save Show a pager for the items Show pager Specific nodes The model to filter by Title Title of this section. Not necesarily visible Type the name of the template to use, it must be inside a folder called atlfinder. Type the node title to autocomplete. aspect atlfinder content title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-28 18:18+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Adicionar Avanzado Filtrar por clasificación Básico Cancelar Filtro por directorio Modo de visualización del contenido Inicio Elementos a mostrar por página Elementos por página Idioma Filtrar por idioma. No seleccione ningún idioma para incluir todos los contenidos Listado de materiales Modo Tipo de contenido Material Directorio Por favor, corrija los errores mostrados a continuación  Guardar Mostrar paginador para los contenidos Mostrar paginador Materiales específicos Filtrar por tipo de contenido Título Título de esta sección. No necesariamente visible. Nombre de la plantilla a utilizar. Debe estar localizada en una carpeta llamada atlfinder Escriba el título del material deseado y se autocompletará con materiales que cumplan con dicho título Clasificación Contenido título 