__author__ = 'rmc'

from django.core.management.base import BaseCommand, CommandError
from atlcore.contenttype.models import Node
from atlcore.plugins.atlfinder.utils.itersubclasses import itersubclasses
from atlcore.plugins.atlfinder.models import ATLFinderFields


class Command(BaseCommand):
    help = 'update altcore model fields'

    def handle(self, *args, **options):
        self.stdout.write("Loading new fields", ending='\n')
        fields = {}
        fields['Node'] = Node._meta.get_all_field_names()
        for model in itersubclasses(Node):
            fields[model.__name__] = model._meta.get_all_field_names()
            for parent in model._meta.get_parent_list():
                fields[model.__name__] = [x for x in list(set(fields[model.__name__]) - set(parent._meta.get_all_field_names())) if not x.endswith('_ptr')]
        tmp = []
        for f in ATLFinderFields.objects.all():
            tmp.append(ATLFinderFields(name=f.name, contentype=f.contentype))

        for type, group in fields.items():
            for attr in group:
                field = ATLFinderFields(name=attr, contentype=type)
                if field not in tmp:
                    field.save()