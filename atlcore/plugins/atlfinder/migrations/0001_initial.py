# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ATLFinderFields'
        db.create_table(u'atlfinder_atlfinderfields', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('contentype', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal(u'atlfinder', ['ATLFinderFields'])

        # Adding model 'ATLFinder'
        db.create_table(u'cmsplugin_atlfinder', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('model', self.gf('django.db.models.fields.CharField')(default='News', max_length=256)),
            ('container', self.gf('django.db.models.fields.CharField')(max_length=36, null=True, blank=True)),
            ('format', self.gf('django.db.models.fields.CharField')(default='c', max_length=1)),
            ('show_paginator', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('content_language', self.gf('django.db.models.fields.CharField')(default='', max_length=5, blank=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(default=15)),
            ('template', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('main_content', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('link_content', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('atlfinder', ['ATLFinder'])

        # Adding M2M table for field aspect on 'ATLFinder'
        db.create_table(u'atlfinder_atlfinder_aspect', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('atlfinder', models.ForeignKey(orm['atlfinder.atlfinder'], null=False)),
            ('aspect', models.ForeignKey(orm[u'aspect.aspect'], null=False))
        ))
        db.create_unique(u'atlfinder_atlfinder_aspect', ['atlfinder_id', 'aspect_id'])

        # Adding M2M table for field exclude on 'ATLFinder'
        db.create_table(u'atlfinder_atlfinder_exclude', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_atlfinder', models.ForeignKey(orm['atlfinder.atlfinder'], null=False)),
            ('to_atlfinder', models.ForeignKey(orm['atlfinder.atlfinder'], null=False))
        ))
        db.create_unique(u'atlfinder_atlfinder_exclude', ['from_atlfinder_id', 'to_atlfinder_id'])

        # Adding M2M table for field fields on 'ATLFinder'
        db.create_table(u'atlfinder_atlfinder_fields', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('atlfinder', models.ForeignKey(orm['atlfinder.atlfinder'], null=False)),
            ('atlfinderfields', models.ForeignKey(orm[u'atlfinder.atlfinderfields'], null=False))
        ))
        db.create_unique(u'atlfinder_atlfinder_fields', ['atlfinder_id', 'atlfinderfields_id'])

        # Adding model 'ATLFinderContent'
        db.create_table(u'atlfinder_atlfindercontent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('node_id', self.gf('django.db.models.fields.CharField')(max_length=36)),
            ('node_title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('model', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('plugin', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['atlfinder.ATLFinder'])),
            ('weight', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('atlfinder', ['ATLFinderContent'])


    def backwards(self, orm):
        # Deleting model 'ATLFinderFields'
        db.delete_table(u'atlfinder_atlfinderfields')

        # Deleting model 'ATLFinder'
        db.delete_table(u'cmsplugin_atlfinder')

        # Removing M2M table for field aspect on 'ATLFinder'
        db.delete_table('atlfinder_atlfinder_aspect')

        # Removing M2M table for field exclude on 'ATLFinder'
        db.delete_table('atlfinder_atlfinder_exclude')

        # Removing M2M table for field fields on 'ATLFinder'
        db.delete_table('atlfinder_atlfinder_fields')

        # Deleting model 'ATLFinderContent'
        db.delete_table(u'atlfinder_atlfindercontent')


    models = {
        u'aspect.aspect': {
            'Meta': {'object_name': 'Aspect'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'parent': ('atlcore.aspect.fields.AtlAspectForeignKeyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['aspect.Aspect']"}),
            'sites': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'aspect_related'", 'symmetrical': 'False', 'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'atlfinder.atlfinder': {
            'Meta': {'object_name': 'ATLFinder', 'db_table': "u'cmsplugin_atlfinder'", '_ormbases': ['cms.CMSPlugin']},
            'aspect': ('atlcore.aspect.fields.AtlAspectField', [], {'symmetrical': 'False', 'related_name': "'atlfinder_related'", 'blank': 'True', 'to': u"orm['aspect.Aspect']"}),
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'container': ('django.db.models.fields.CharField', [], {'max_length': '36', 'null': 'True', 'blank': 'True'}),
            'content_language': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '5', 'blank': 'True'}),
            'exclude': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'atlfinder_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['atlfinder.ATLFinder']"}),
            'fields': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['atlfinder.ATLFinderFields']", 'null': 'True', 'blank': 'True'}),
            'format': ('django.db.models.fields.CharField', [], {'default': "'c'", 'max_length': '1'}),
            'link_content': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'main_content': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'model': ('django.db.models.fields.CharField', [], {'default': "'News'", 'max_length': '256'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '15'}),
            'show_paginator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'atlfinder.atlfindercontent': {
            'Meta': {'ordering': "['weight']", 'object_name': 'ATLFinderContent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'node_id': ('django.db.models.fields.CharField', [], {'max_length': '36'}),
            'node_title': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'plugin': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['atlfinder.ATLFinder']"}),
            'weight': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'atlfinder.atlfinderfields': {
            'Meta': {'object_name': 'ATLFinderFields'},
            'contentype': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['atlfinder']