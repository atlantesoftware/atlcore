# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ATLFinder.aspect'
        db.delete_column(u'cmsplugin_atlfinder', 'aspect_id')


    def backwards(self, orm):
        # Adding field 'ATLFinder.aspect'
        db.add_column(u'cmsplugin_atlfinder', 'aspect',
                      self.gf('atlcore.aspect.fields.AtlAspectForeignKeyField')(related_name='atlfinder_related', null=True, to=orm['aspect.Aspect'], blank=True),
                      keep_default=False)


    models = {
        'atlfinder.atlfinder': {
            'Meta': {'object_name': 'ATLFinder', 'db_table': "u'cmsplugin_atlfinder'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'container': ('django.db.models.fields.CharField', [], {'max_length': '36', 'null': 'True', 'blank': 'True'}),
            'exclude': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'atlfinder_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['atlfinder.ATLFinder']"}),
            'format': ('django.db.models.fields.CharField', [], {'default': "'l'", 'max_length': '1'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '15'}),
            'show_paginator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'atlfinder.atlfindercontent': {
            'Meta': {'ordering': "['weight']", 'object_name': 'ATLFinderContent', 'db_table': "'d_atlfinder_content'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'node_id': ('django.db.models.fields.CharField', [], {'max_length': '36'}),
            'plugin': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['atlfinder.ATLFinder']"}),
            'weight': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        }
    }

    complete_apps = ['atlfinder']