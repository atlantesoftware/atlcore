#coding=UTF-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from atlcore.contenttype.models import Node
# from models import ATLFinder
from utils.itersubclasses import itersubclasses
from cms.models import CMSPlugin
from atlcore.aspect.fields import AtlAspectField
from atlcore.aspect.models import Aspect
from django.conf import settings
import os

FORMATS = (
    ('c', _('Specific nodes')), #Content
    ('l', _('List')),
)
#load atlcore models
MODELS = []
for model in itersubclasses(Node):
    MODELS.append((model.__name__, model.__name__))
#end load atlcore models

LANGUAGES = settings.LANGUAGES

# LANGUAGE_CODE = 'es'

#load templates
TEMPLATES = []
dir = []
for app in settings.INSTALLED_APPS:
    app = __import__(app)
    dir.append(app.__path__[0])
dir = set(dir)

for rootDir in dir:
    for dirName, subdirList, fileList in os.walk(rootDir):
        if 'templates%satlfinder' % os.sep in dirName:
            for fname in fileList:
                if fname.endswith('.html'):
                    TEMPLATES.append((fname, fname.replace('.html', '').replace('_', ' ').title()))
#end load templates

class ATLFinderFields(models.Model):
    name = models.CharField(_('Name'), max_length=256)
    contentype = models.CharField(_('Contentype'), max_length=256)

    def __unicode__(self):
        return '%s - %s' % (self.contentype, self.name)


class ATLFinder(CMSPlugin):
    title = models.CharField(_('Title'), max_length=256, help_text=_("Title of this section. Not necesarily visible"))
    model = models.CharField(_('Model'), max_length=256, choices=MODELS, default='News',
                             help_text=_("The model to filter by"))
    container = models.CharField(_("Parent folder"), max_length=36, null=True, blank=True,
                                 help_text=_("Container folder to filter by"))
    order_by = models.CharField(_("Order by"), max_length=50, null=True, blank=True, help_text=_("Python code to order by"))
    aspect = AtlAspectField(Aspect, related_name='%(class)s_related', verbose_name=_('aspect'), blank=True,
                            help_text=_("Aspect to filter by"))
    format = models.CharField(_('Mode'), max_length=1, choices=FORMATS, default='c', help_text=_("Display mode"))
    show_paginator = models.BooleanField(_('Show pager'), default=False, help_text=_("Show a pager for the items"))
    content_language = models.CharField(_('Language'), max_length=5, choices=LANGUAGES,
                                        default='', blank=True,
                                        help_text=_("Language to filter by. Leave unselected to include all languages"))
    quantity = models.IntegerField(_("Items per page"), default=15, help_text=_("How many items to show per page"))
    exclude = models.ManyToManyField("ATLFinder", verbose_name=_('Exclude'), related_name='%(class)s_related',
                                     # symmetrical=False,
                                     null=True, blank=True,
                                     limit_choices_to={'placeholder__page__publisher_is_draft': False},
                                     help_text=_(
                                         "Select other instances of this plugin whose result you want to exclude from this query."))
    template = models.CharField(_('Template'), max_length=256, choices=TEMPLATES,
                                help_text=_(
                                    'Type the name of the template to use, it must be inside a folder called atlfinder.'))

    fields = models.ManyToManyField(ATLFinderFields, verbose_name=_('Exclude fields'), null=True, blank=True)

    main_content = models.BooleanField(_('Main content'), default=False)
    link_content = models.BooleanField(_('Link content'), default=True)

    css_class = models.CharField(_('CSS class'), max_length=256, null=True, blank=True,
                                 help_text=_('css classes separated by space'))

    def _get_templates(self):
        pass

    def __unicode__(self):
        return self.title

    def copy_relations(self, old_instance):
        self.aspect = old_instance.aspect.all()
        self.fields = old_instance.fields.all()
        self.exclude = old_instance.exclude.all()
        for atlfindercontent in old_instance.atlfindercontent_set.all():
            atlfindercontent.pk = None
            atlfindercontent.plugin = self
            atlfindercontent.save()

    def get_ancestors(self):
        if self.aspect:
            return self.aspect.get_ancestors() + [self.aspect]
        return []

    class Meta:
        app_label = 'atlfinder'


class ATLFinderContent(models.Model):
    node_id = models.CharField(max_length=36)
    node_title = models.CharField(_('Node'), max_length=256,
                                  help_text=_('Type the node title to autocomplete.'))
    model = models.CharField(_('Model'), max_length=256, help_text=_("The model to filter by"))
    plugin = models.ForeignKey(ATLFinder)
    weight = models.IntegerField(default=0)

    def __unicode__(self):
        return self.node_title

    class Meta:
        app_label = 'atlfinder'
        verbose_name = _('node')
        verbose_name_plural = _('nodes')
        ordering = ['weight']