jQuery(document).ready(function () {

    toggle($('#id_format').val());

    update_model($('#id_model'));

    $('#id_show_paginator').change(function () {
        if ($(this).is(':checked'))
            $('.field-quantity').show();
        else
            $('.field-quantity').hide();
    })

    $('#id_format').change(function () {
        toggle($(this).val());
    })

    $('#id_model').change(function () {
        update_model($(this));
    })

    $('#atlfindercontent_set-group .add-row a').click(function () {
        update_autocomplete();
    })

    update_autocomplete();
})

function toggle(type) {
    if (type == 'c') {
        $('.field-show_paginator, .field-quantity, .field-exclude, .field-content_language, .field-aspect, .field-container, .field-model').hide();
        $('.field-items, #atlfindercontent_set-group').show();
    }
    else {
        $('.field-show_paginator, .field-quantity, .field-exclude, .field-content_language, .field-aspect, .field-container, .field-model').show();
        $('.field-items, #atlfindercontent_set-group').hide();
    }
}

function update_model(model) {
    $('#atlfindercontent_set-group .field-model').each(function () {
        if ($(this).parent().find('.field-node_title input').val() != '') {
            $(this).find('input').val(model.val());
        }
    })
}

function update_autocomplete() {
    var url = "/atlfinder/search/";
    $(".field-node_title input").autocomplete({
        source: url,
        minLength: 2,
        select: function (event, ui) {
            update_model($('#id_model'));
            $(this).parent().parent().parent().find('.field-node_id input').val(ui.item.id)
        }
    });
}

//cambio temporal