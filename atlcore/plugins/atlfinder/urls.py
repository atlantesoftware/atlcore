from django.conf.urls import patterns, include, url

# urlpatterns = patterns('',
#                        # url(r'^atlfinder/filters/$', 'plugins.atlfinder.views.filters', name='filters'),
#                        url(r'^atlfinder/filters/$', 'plugins.atlfinder.views.filters', name='filters'),
#                        # url(r'^', 'plugins.atlfinder.views.filters', name='filters'),
# )


urlpatterns = patterns('atlcore.plugins.atlfinder.views',
                       # url(r'^json_objects/(?P<model>.+)/$', 'json_objects', name='json-objects'),
                       url(r'^search/(?P<container>.+)/$', 'atlfinder_search', name='atlfinder-search'),
                       url(r'^search/$', 'atlfinder_search', name='atlfinder-search'),
)