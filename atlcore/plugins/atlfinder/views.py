# -*- coding: utf-8 -*-
from django.http import HttpResponse
# from django.db.models import get_model
from atlcore.contenttype.models import Node
from django.utils import translation
from django.utils.translation import ugettext as _
import json


# def json_objects(request, model):
#     q = request.GET.get('term', None)
#
#     field = 'title'
#     objects = []
#     # model = get_model('contenttype', model)
#     cur_language = translation.get_language()
#
#     objs = model.objects.filter(**{'%s__icontains' % field: q}).filter(state='Public', language=cur_language).order_by(
#         '-title')
#
#     # objs = Node.objects.filter(**{'%s__icontains' % field: q}).filter(state='Public', language=cur_language).order_by(
#     #     '-title')
#     for obj in objs:
#         field_val = getattr(obj, field, '')
#         if field_val is not None:
#             objects.append({
#                 'id': obj.id,
#                 'label': '%s (%s)' % (field_val, _(obj._meta.module_name)),
#                 'value': field_val,
#             })
#
#     return HttpResponse(json.dumps(objects), mimetype='application/json')


def atlfinder_search(request, container=None):
    if request.is_ajax():
        q = request.GET.get('term', '')
        query = {}
        query['title__icontains'] = q
        query['state'] = 'Public'
        if (container):
            query['parent'] = container
        objs = Node.objects.filter(**query).filter(state='Public').order_by('-title')[:20]
        results = []
        for obj in objs:
            json_obj = {}
            json_obj['id'] = obj.id
            json_obj['label'] = '%s (%s)' % (obj.title, obj.language)
            json_obj['value'] = '%s (%s)' % (obj.title, obj.language)
            results.append(json_obj)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
