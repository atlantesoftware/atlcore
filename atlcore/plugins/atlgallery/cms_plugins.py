#coding=UTF-8
from django.utils.translation import ugettext as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from atlcore.plugins.atlgallery.models import ATLGallery

class ATLGalleryPlugin(CMSPluginBase):
    model = ATLGallery # Model where data about this plugin is saved
    name = _("ATL Gallery") # Name of the plugin
    render_template = "atlplugin_gallery/plugin.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
        context.update({'instance':instance})
        return context


plugin_pool.register_plugin(ATLGalleryPlugin) # register the plugin
