#coding=UTF-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from atlcore.contenttype.models import Container as ContainerContentType


class ATLGallery(CMSPlugin):
    title = models.CharField('Title', max_length=256, default=None, null=True)
    container = models.ForeignKey(ContainerContentType)
    show_title = models.BooleanField(_('Show title'), default=True)
    show_description = models.BooleanField(_('Show description'), default=False)

    def __unicode__(self):
        return self.container.title
