#coding=UTF-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from atlcore.contenttype.models import Video as VideoContenttype
from atlcore.plugins.atlvideo import plugin_settings


class ATLVideo(CMSPlugin):
    title = models.CharField('Title', max_length=256, default=None, null=True)
    video = models.ForeignKey(VideoContenttype, related_name = 'atlvideo_plugin')
    show_player = models.BooleanField(_('Show player'), default=False)
    show_title = models.BooleanField(_('Show title'), default=True)
    show_description = models.BooleanField(_('Show description'), default=False)
    show_body = models.BooleanField(_('Show body'), default=False)
    width = models.PositiveSmallIntegerField(_('width'), default=plugin_settings.VIDEO_WIDTH)
    height = models.PositiveSmallIntegerField(_('height'), default=plugin_settings.VIDEO_HEIGHT)


    def __unicode__(self):
        return self.video.title
