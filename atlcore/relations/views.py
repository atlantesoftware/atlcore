#coding=UTF-8
import json
from atlcore.relations import helper

from django.http import HttpResponse


def json_objects(request, model_list):
    q = request.GET.get('term', None)
    objects = []
    models_name = model_list.split(',')
    if q:
        objects = helper.get_list(models_name, q)  
    return HttpResponse(json.dumps(objects), mimetype='application/json')
