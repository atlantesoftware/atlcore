from django.contrib.admin.util import unquote
from django.contrib import admin
from django.db.models.base import ModelBase
from django.contrib.admin import ModelAdmin


from cms.utils.conf import get_cms_setting

from atlcore.contenttype.models import Node
from atlcore.utils.models import copy_model_instance
from atlcore.translations.utils import get_language_from_request,TranslationManager
from atlcore.translations import constants


class TranslationAdminMixin(object):
    change_form_template = 'admin/cms/trans_change_form.html'
    add_form_template = 'admin/cms/trans_add_form.html'

    def add_to_container_view(self, request, container_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context.update({'show_language_tabs':False})
        return super(TranslationAdminMixin, self).add_to_container_view(request,container_id=container_id,form_url=form_url, extra_context=extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        #Getting translation source
        trans_id =  request.GET.get('trans','')
        language = get_language_from_request(request)
        extra_context = extra_context or {}
        extra_context.update({'show_language_tabs':False})
        try:
            trans_obj = self.get_object(request, unquote(trans_id))
        except self.model.DoesNotExist:
            # Don't raise Http404 just yet, because we haven't checked
            # permissions yet. We don't want an unauthenticated user to be able
            # to determine whether a given object exists.
            trans_obj = None
        if trans_obj:
            self._update_container_id(request,trans_obj)
            mock_translation = self._get_new_translation(trans_obj,language)
            extra_context.update(self.get_language_tab_context(request,mock_translation))
            #extra_context.update(self.get_unihandecode_context(language))
        return super(TranslationAdminMixin, self).add_view(request, form_url, extra_context=extra_context)

    def _get_new_translation(self,trans_base,language):
        mock_translation = copy_model_instance(trans_base)
        mock_translation.id = trans_base.id
        mock_translation.language = language
        mock_translation.get_relation_objects = lambda *args,**kwargs : [trans_base] + trans_base.get_relation_objects(constants.TRANSLATION_RELATION_SLUG)
        return mock_translation


    def _update_container_id(self,request,trans_obj):
        if trans_obj and trans_obj.parent:
            container_id = trans_obj.parent.id
            request.session['container_id'] = container_id

    def get_relations_types_and_instances(self,obj):
        from atlcore.relations.models import AtlRelation, AtlRelationsInstance
        relations = AtlRelationsInstance.objects.for_this_object_with_pos(obj)
        f_relations = filter(lambda (relation_instance,pos): relation_instance.relation.slug != constants.TRANSLATION_RELATION_SLUG,relations)
        relation_types = AtlRelation.objects.for_this_class_with_pos(obj.__class__)
        f_relation_types = filter(lambda (relation,pos): relation.slug != constants.TRANSLATION_RELATION_SLUG,relation_types)
        return f_relation_types, f_relations

    def change_view(self, request, object_id,extra_context=None):
        """
        The 'change' admin view for the Node model.
        """
        if extra_context is None:
            extra_context = {}

        try:
            obj = self.get_object(request, unquote(object_id))
        except self.model.DoesNotExist:
            # Don't raise Http404 just yet, because we haven't checked
            # permissions yet. We don't want an unauthenticated user to be able
            # to determine whether a given object exists.
            obj = None

        if obj:
            extra_context.update(self.get_language_tab_context(request,obj))
        #tab_language = get_language_from_request(request)
        #extra_context.update(self.get_unihandecode_context(tab_language))
        return super(TranslationAdminMixin, self).change_view(request, object_id, extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        super(TranslationAdminMixin,self).save_model(request, obj, form, change)
        if not change:
            language =  request.GET.get('language','')
            if obj.language == language:
                trans_id =  request.GET.get('trans','')
                main_obj =  self.get_object(request, unquote(trans_id))
                TranslationManager.add_translation(main_obj,obj)


    def get_language_tab_context(self, request, obj):
        context = {}
        language = get_language_from_request(request, obj)
        languages = TranslationManager.get_language_tabs_for(obj,language)
        context.update({
            'language': language,
            'language_tabs': languages,
            'show_language_tabs': len(languages) > 1,
        })
        return context

    def get_unihandecode_context(self, language):
        if language[:2] in get_cms_setting('UNIHANDECODE_DECODERS'):
            uhd_lang = language[:2]
        else:
            uhd_lang = get_cms_setting('UNIHANDECODE_DEFAULT_DECODER')
        uhd_host = get_cms_setting('UNIHANDECODE_HOST')
        uhd_version = get_cms_setting('UNIHANDECODE_VERSION')
        if uhd_lang and uhd_host and uhd_version:
            uhd_urls = [
                '%sunihandecode-%s.core.min.js' % (uhd_host, uhd_version),
                '%sunihandecode-%s.%s.min.js' % (uhd_host, uhd_version, uhd_lang),
            ]
        else:
            uhd_urls = []
        return {'unihandecode_lang': uhd_lang, 'unihandecode_urls': uhd_urls}

from atlcore.relations.models import AtlRelation,AtlRelationsInstance
from atlcore.relations.admin import AtlRelationAdmin,AtlRelInstanceAdmin

class TranslationHiddenAtlRelationAdmin(AtlRelationAdmin):
    def queryset(self, request):
        qs = super(TranslationHiddenAtlRelationAdmin, self).queryset(request)
        return qs.exclude(slug = constants.TRANSLATION_RELATION_SLUG)

class TranslationHiddenAtlRelationInstanceAdmin(AtlRelInstanceAdmin):
    def queryset(self, request):
        qs = super(TranslationHiddenAtlRelationInstanceAdmin, self).queryset(request)
        return qs.exclude(relation__slug = constants.TRANSLATION_RELATION_SLUG)


from atlcore.utils import class_for_path

class TranslationSite(object):

    @classmethod
    def _is_admin_registered_for_translation(cls,ModelAdminKlass):
        return issubclass(ModelAdminKlass,TranslationAdminMixin) or issubclass(ModelAdminKlass,TranslationHiddenAtlRelationAdmin) or issubclass(ModelAdminKlass,TranslationHiddenAtlRelationInstanceAdmin)

    @classmethod
    def _get_class(cls,class_path):
        try:
            model_klass = class_for_path(class_path)
        except Exception:
            model_klass = None
        return model_klass

    @classmethod
    def in_excluded_models(cls,model_class,excluded_list):
        excluded_models_list =[model for model in map(cls._get_class,excluded_list) if model is not None ]
        excluded_models = tuple(excluded_models_list)
        return issubclass(model_class,excluded_models)

    @classmethod
    def register_all(cls):
        _registry_keys = list(admin.site._registry.iterkeys())
        for ModelClass in _registry_keys:
            if issubclass(ModelClass,Node) and not cls.in_excluded_models(ModelClass,constants.TRANSLATION_EXCLUDED_MODELS):
                ModelAdminClass = admin.site._registry[ModelClass].__class__
                cls.register(ModelClass,ModelAdminClass)

    @classmethod
    def register(cls, model_or_iterable, admin_class=None):
        if not admin_class:
            admin_class = ModelAdmin

        if isinstance(model_or_iterable, ModelBase):
            model_or_iterable = [model_or_iterable]

        for model in model_or_iterable:
            if issubclass(model,Node) and not cls.in_excluded_models(model,constants.TRANSLATION_EXCLUDED_MODELS):
                if not cls.in_excluded_models(admin_class,constants.TRANSLATION_EXCLUDED_MODELADMINS):
                    admin_class.__bases__ = (TranslationAdminMixin,) + admin_class.__bases__
                    try:
                        admin.site.unregister(model)
                        admin.site.register(model,admin_class)
                    except Exception:
                        pass

admin.site.unregister(AtlRelation)
admin.site.unregister(AtlRelationsInstance)

admin.site.register(AtlRelation, TranslationHiddenAtlRelationAdmin)
admin.site.register(AtlRelationsInstance, TranslationHiddenAtlRelationInstanceAdmin)


