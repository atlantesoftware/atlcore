from django.utils.translation import get_language
from django.conf import  settings

LANGUAGES = getattr(settings,'LANGUAGES',(get_language(),))
TRANSLATION_RELATION_SLUG =  getattr(settings,'TRANSLATION_RELATION_SLUG','translation')

TRANSLATION_EXCLUDED_MODELS = getattr(settings,'TRANSLATION_EXCLUDED_MODELS',['atlcore.contenttype.models.Folder'])
TRANSLATION_EXCLUDED_MODELADMINS = ['atlcore.translations.admin.TranslationAdminMixin','atlcore.translations.admin.TranslationHiddenAtlRelationAdmin','atlcore.translations.admin.TranslationHiddenAtlRelationInstanceAdmin'] + getattr(settings,'TRANSLATION_EXCLUDED_MODELADMINS',[])
