from classytags.arguments import Argument, StringArgument,Flag
from classytags.core import Options
from classytags.helpers import InclusionTag

from django import template
from django.utils import translation

register = template.Library()

class LanguageMenu(InclusionTag):
    """
    render a language menu with custom template and options
    - paginator: paginator object from context
    - template: template used to render the menu
    - show_page_indicator: if page indicator is showed
    """
    name = 'language_menu'
    template = 'translations/language_menu.html'

    options = Options(
        Flag('show_page_indicator',default=True,false_values=['false', 'no']),
        Argument('paginator', default=None, required=False),
        StringArgument('template', default='translations/language_menu.html', required=False)
    )

    def get_template(self, context,show_page_indicator, paginator, template):
        if (template != self.template) and (template!="") :
            self.template = template
        return self.template

    def get_context(self, context,show_page_indicator, paginator, template):
        context_paginator = paginator or context['paginator']
        current_language = translation.get_language()
        translation.LA
        context.update({'current_language': context_paginator,
                        'show_page_indicator': show_page_indicator
        })

        return context

register.tag(LanguageMenu)
