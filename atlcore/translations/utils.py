from django.utils.translation import get_language,get_language_info
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from atlcore.contenttype.models import Node
from .constants import LANGUAGES,TRANSLATION_RELATION_SLUG

def get_language_from_request(request, node = None):
    """
    Return the most obvious language according the request
    """
    if node:
        object_language = node.language
    else:
        object_language = None
    language = object_language or request.REQUEST.get('language', None) or get_language()
    return language

def get_languages_info():
    all_languages = []
    for language in LANGUAGES:
        all_languages.append(get_language_info(language[0]))
    return all_languages

def get_languages():
    languages = []
    languages_info = get_languages_info()
    for info in languages_info:
        languages.append((info['code'], info['name']))
    return languages

from atlcore.relations.models import AtlRelationsInstance,AtlRelation

class TranslationManager(object):

    @classmethod
    def create_relation(cls,relation_slug = ""):
        if relation_slug == "":
            relation_slug = TRANSLATION_RELATION_SLUG
        relation,created = AtlRelation.objects.get_or_create(slug=relation_slug,defaults=cls.default_relation_attributes())
        if created:
            relation.content_types_group1 = cls.get_relation_content_types_group()
            relation.content_types_group2 = cls.get_relation_content_types_group()
        return relation,created

    @classmethod
    def add_translation(cls,obj,translation_obj):
        relation,created = cls.create_relation()
        AtlRelationsInstance.objects.new(relation,translation_obj,obj)


    @classmethod
    def default_relation_attributes(cls):
        defaults = { 'title' :  _(TRANSLATION_RELATION_SLUG),
                     'description': _("Translation Relation"),
                     'is_bidirectional': True
            }
        return defaults

    @classmethod
    def get_relation_content_types_group(cls):
        return [ct.pk for ct in ContentType.objects.all()]


    @classmethod
    def get_translations(cls,obj):
       translations = {info['code']:None for info in get_languages_info() if info['code'] != get_language()}
       if not obj:
           return translations
       try:
           trans_list = obj.get_relation_objects(relation_slug=TRANSLATION_RELATION_SLUG)
       except :
           trans_list = []
       trans = {trans.language: trans for trans in trans_list}
       translations.update(trans)
       return translations


    @classmethod
    def check_relation(cls,relation_slug=""):
        """
            Check if relation exists if not return None else returns AtlRelation object
        """
        relations = AtlRelation.objects.filter(slug = relation_slug)
        if len(relations > 0):
            return relations[0]
        else:
            return None

    @classmethod
    def get_language_tabs_for(cls,obj,lang):
        languages = get_languages_info()
        translations = cls.get_translations(obj)
        for language in languages:
            if language['code'] == lang:
                language['url'] = '#'
            else:
                trans_obj =  translations.get(language['code'],None) if language['code'] != obj.language else obj
                language['url'] ='%s?trans=%s&language=%s' % (obj.admin_add_url(),obj.id,language['code']) if not trans_obj else trans_obj.admin_change_url()
        return languages

    @classmethod
    def get_language_tabs(cls,obj):
        languages = get_languages_info()
        translations = cls.get_translations(obj)
        for language in languages:
            if language['code'] == obj.language:
                language['url'] = '#'
            else:
                trans_obj =  translations.get(language['code'],None)
                language['url'] ='%s?trans=%s&language=%s' % (obj.admin_add_url(),obj.id,language['code']) if not trans_obj else trans_obj.admin_change_url()
        return languages


