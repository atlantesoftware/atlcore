#coding=UTF-8
from django.utils.importlib import import_module

def class_for_path(class_path):
    module_name, class_name = class_path.rsplit(".", 1)
    m = import_module(module_name)
    c = getattr(m, class_name)
    return c

def get_class(class_path):
    try:
        model_klass = class_for_path(class_path)
    except Exception:
        model_klass = None
    return model_klass