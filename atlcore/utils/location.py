'''
Created on Aug 19, 2013

@author: allenson
'''
import os


def get_atl_path():
    import atlcore
    return os.path.abspath(atlcore.__path__[0])


def get_atl_base_path():
    atl_path = get_atl_path()
    return os.path.split(atl_path)[0]


def is_atl_embedded(target_path):
    return target_path == get_atl_base_path()


def get_project_default_base_path():
    current_script_path = os.getcwd()
    atl_path = get_atl_path()
    #is inside atl
    if current_script_path == atl_path:
        return get_atl_base_path()
    else:
        return current_script_path