__author__ = 'allen'
import abc
import sys



class BaseProfiler(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self,**options):
        """
            Initialize a profiler
        """
        self._options = options

    def _error(*args):
        for arg in args:
            print >>sys.stderr,  arg,
        print >>sys.stderr

    def _internal_request(self,url):
        """
            Request page with internal Django client.
        """
        from django.test.client import Client
        # many code assume request.META['REMOTE_ADDR'] and etc
        client = Client(REMOTE_ADDR="127.0.0.1", HTTP_HOST="localhost")
        try:
            resp = client.get(url)
        except Exception, e:
            self._error('url=%s error=%s' % (url, e))
            resp = type('object', (), {'status_code':0, 'content': str(e)})

        #if resp.status_code in (500,) and options.save_errors:
            #save_page(resp.content, url, options.save_errors)
        return resp


    @abc.abstractmethod
    def profile(self,target):
        """
            Profile a target
        """

class SQLProfiler(BaseProfiler):
    """
        Find SQL queries usage for each page
    """

    def __init__(self,**options):
        super(SQLProfiler,self).__init__(**options)

    def profile(self,url):
        verbosity = self._options.get('verbosity',0)
        if verbosity:
            print "profile_sql", url,
        from django.conf import settings
        old_debug = settings.DEBUG
        settings.DEBUG = True
        from django.db import connection
        connection.queries = []
        resp = self._internal_request(url)
        if verbosity:
            print "%d SQL queries, status code: %s " %\
                  (len(connection.queries), resp.status_code)

        total_time = 0.0
        for query in connection.queries:
            total_time = total_time + float(query['time'])
            if verbosity > 1:
                print query['sql'], query['time']

        settings.DEBUG = old_debug
        return {'Path': url,'Total SQL Queries': len(connection.queries),'Total SQL Time': total_time, 'Status': resp.status_code,}

class SizeProfiler(BaseProfiler):
    """
        Find size of each page.
    """

    def __init__(self,**options):
        super(SizeProfiler,self).__init__(**options)

    def profile(self,target):
        pass