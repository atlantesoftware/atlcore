#!/usr/bin/env python
import sys
import os

def add_parent_dir():
    atl_dir = os.path.abspath(os.path.dirname(__file__))
    parent_dir = os.path.split(atl_dir)[0]
    if parent_dir not in sys.path:
        sys.path.insert(0,parent_dir)

if __name__ == "__main__":
    #Adding ATL base path
    add_parent_dir()
    import atlcore.management as management
    management.execute_from_command_line()