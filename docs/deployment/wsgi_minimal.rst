#####################################
Deployment using Apache and mod_wsgi
#####################################

El deploy y configuración de los sitios web no es 100% portable debido a la
dependencia de librerias externas de algunos paquetes python usados por el ATL Framework (Pillow,lxml,etc)
y por consiguiente por el sitio web.

**************************
Instalar Apache + mod_wsgi
**************************

Apache:
=======

Instalación::

    $ sudo apt-get install apache2 apache2.2-common apache2-mpm-prefork apache2-utils libexpat1


mod_wsgi:
=========

Instalación::

    $ sudo apt-get install libapache2-mod-wsgi

reiniciar apache (apache2 restart)

*****************************************************************
Instalación de los paquetes python que usa el sitio(dependencias)
*****************************************************************

Para los servidores cuyo acceso es a través de ftp y no ssh (ej ``Cubarte``)
el método recomendado es `No Virtualenv(Copy & Paste)`_.

No VirtualEnv(Copy & Paste)
===========================

Copiar la carpeta que contiene los paquetes python (ej: ``.virtualenvs/icm/lib/python2.7/site-packages``)
en un lugar que se pueda referenciar mas adelante por el fichero de configuración del apache,
se recomienda copiar esta carpeta dentro de la carpeta raíz del sitio,
en un futuro se pudieran agregar otros paquetes y es más fácil para el mantenimiento y soporte.
Tener en cuenta las dependencias de estos paquetes.(Ver `Dependencias de Librerías Externas`_)

VirtualEnv
===========

..

TODO

En el S.O basado en el interprete de python x defecto
=====================================================

..

Opción menos saludable
TODO

*******************
Configurar mod_wsgi
*******************

En el fichero de configuración del apache (ya sea en el http.conf o en vhost específicos)
añadir lo siguiente:

.. code-block:: apacheconf


    #Alias de la carpeta con las medias, en este caso es la carpeta files dentro de la raíz
    #Acorde con la configuración del sitio django(settings.py)

    Alias /files/ /var/www/vhosts/icm.cult.cu/files/

    #Verificar permisos específicos de lectura en la carpeta con las medias(si se requiere)
    <Directory /var/www/vhosts/icm.cult.cu/files>

        Order deny,allow

        Allow from all

    </Directory>


    #Mapeo de la url raiz con el fichero wsgi.py
    WSGIScriptAlias / /var/www/vhosts/icm.cult.cu/mysite/wsgi.py

    #WSGI como demonio, recomendado para servers no windows
    #En el user poner el user especifico vinculado con el sitio
    #python-path:el path a la carpeta raiz del sitio:path a la carpeta de las dependencias (site-packages)
    WSGIDaemonProcess icmsite user=userx threads=15 display-name=%{GROUP} python-path=/var/www/vhosts/icm.cult.cu:/var/www/vhosts/icm.cult.cu/site-packages

    WSGIProcessGroup icmsite

    #Verificar permisos específicos de lectura para wsgi.py
    <Directory /var/www/vhosts/icm.cult.cu/mysite>

                 <Files wsgi.py>

                            Order deny,allow

                            Allow from all

                    </Files>

    </Directory>


Después reiniciar apache(apache2 restart)

**********************************
Dependencias de Librerías Externas
**********************************


Python
======

.. code-block:: console

    $ sudo apt-get install python-dev python-setuptools


MySQL-python
============

Requirements
------------

libmysqlclient

.. code-block:: console

    $ sudo apt-get install libmysqlclient-dev


lxml
====

Requirements
------------

libxml2
libxslt

.. code-block:: console

    $ sudo apt-get install libxml2-dev libxslt-dev

Ver más en `lxml`_

.. _lxml: http://lxml.de/installation.html#requirements


Pillow
======

Requirements
------------

libjpeg: provides JPEG functionality.
zlib: provides access to compressed PNGs
libtiff: provides group4 tiff functionality
libfreetype: provides type related services
littlecms: provides color management
libwebp: provides the Webp format
tcl/tk: provides support for tkinter bitmap and photo images.

.. code-block:: console

    $ sudo apt-get install libtiff4-dev libjpeg62-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev

Opcional:

.. code-block:: console

    $ sudo apt-get install tcl8.5-dev tk8.5-dev

En última instancia:

.. code-block:: console

    $ sudo apt-get install python-imaging


Ver más en `Pillow`_

.. _Pillow: https://pillow.readthedocs.org/en/latest/installation.html#external-libraries


***********
Referencias
***********

