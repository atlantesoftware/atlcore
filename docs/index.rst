.. ATL documentation master file, created by
   sphinx-quickstart on Mon Jan 20 14:48:24 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

######################################
Welcome to ATL Framework's documentation!
######################################

This document refers to version |release|

*******
Install
*******

.. toctree::
    :maxdepth: 1

    getting_started/installation
    upgrade/2.0.0



**********
Deployment
**********

.. toctree::
    :maxdepth: 2
    :numbered:
       
    deployment/wsgi_minimal
    deployment/sitemap
    deployment/templatetags
    deployment/cli
    deployment/permissions_reference
 


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

