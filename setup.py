from setuptools import setup, find_packages
import os
import sys

from distutils.core import setup
from distutils.sysconfig import get_python_lib

app_name = 'atlcore'

# Warn if we are installing over top of an existing installation. This can
# cause issues where files that were deleted from a more recent Django are
# still present in site-packages. See #18115.
overlay_warning = False
if "install" in sys.argv:
    # We have to try also with an explicit prefix of /usr/local in order to
    # catch Debian's custom user site-packages directory.
    for lib_path in get_python_lib(), get_python_lib(prefix="/usr/local"):
        existing_path = os.path.abspath(os.path.join(lib_path, app_name))
        if os.path.exists(existing_path):
            # We note the need for the warning here, but present it after the
            # command is run, so it's more likely to be seen.
            overlay_warning = True
            break


def fullsplit(path, result=None):
    """
    Split a pathname into components (the opposite of os.path.join)
    in a platform-neutral way.
    """
    if result is None:
        result = []
    head, tail = os.path.split(path)
    if head == '':
        return [tail] + result
    if head == path:
        return result
    return fullsplit(head, [tail] + result)


EXCLUDE_FROM_PACKAGES = ['atlcore.bin']

def is_package(package_name):
    for pkg in EXCLUDE_FROM_PACKAGES:
        if package_name.startswith(pkg):
            return False
    return True


# Compile the list of packages available, because distutils doesn't have
# an easy way to do this.
packages, package_data = [], {}

root_dir = os.path.dirname(__file__)
if root_dir != '':
    os.chdir(root_dir)

for dirpath, dirnames, filenames in os.walk(app_name):
    # Ignore PEP 3147 cache dirs and those whose names start with '.'
    dirnames[:] = [d for d in dirnames if not d.startswith('.') and d != '__pycache__']
    parts = fullsplit(dirpath)
    package_name = '.'.join(parts)
    if '__init__.py' in filenames and is_package(package_name):
        packages.append(package_name)
    elif filenames:
        relative_path = []
        while '.'.join(parts) not in packages:
            relative_path.append(parts.pop())
        relative_path.reverse()
        path = os.path.join(*relative_path)
        package_files = package_data.setdefault('.'.join(parts), [])
        package_files.extend([os.path.join(path, f) for f in filenames])

CLASSIFIERS = [
    'Development Status :: 3 - Alpha',
    'Environment :: Web Environment',
    'Framework :: Django',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    'Topic :: Software Development',
    'Topic :: Software Development :: Libraries :: Application Frameworks',
]


version = __import__(app_name).get_version()

setup(
    author="Atlante Software",
    author_email="admin@atlantesoftware.com",
    name=app_name,
    version=version,
    description='An Advanced Django Framework',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.rst')).read(),
    url='https://www.atlantesoftware.com/',
    license='BSD License',
    platforms=['OS Independent'],
    classifiers=CLASSIFIERS,
    install_requires=[
        'Django>=1.4,<1.6',
        'django-classy-tags>=0.3.4.1',
        'south>=0.7.2',
		'Pillow>=1.7.7',
		'lxml>=3.2.4',
		'Whoosh>=2.5.5',
		'pycrypto>=2.6.1',
        'html5lib',
        'django-mptt>=0.5.1,<0.5.3',
		'django-haystack>=2.1.0',
        'django-sekizai>=0.7',
		'django-tinymce>=1.5.2',
		'python-dateutil>=2.2',
				
    ],
    tests_require=[
        'mock>=1.0.1',
        'django-reversion>=1.6.6',
        'Pillow==1.7.7',
        'Sphinx==1.1.3',
        'Jinja2==2.6',
        'Pygments==1.5',
        'dj-database-url==0.2.1',
        'django-hvad',
    ],
    packages=find_packages(exclude=["project", "project.*"]),
    #packages=packages,
    #package_data=package_data,
    include_package_data=True,
    scripts=['bin/atl-admin.py'],
    zip_safe=False,
    test_suite='runtests.main',
)

if overlay_warning:
    sys.stderr.write("""

========
WARNING!
========

You have just installed Atl Framework over top of an existing
installation, without removing it first. Because of this,
your install may now include extraneous files from a
previous version that have since been removed from
Atl Framework. This is known to cause a variety of problems. You
should manually remove the

%(existing_path)s

directory and re-install Atl Framework.

""" % {"existing_path": existing_path})